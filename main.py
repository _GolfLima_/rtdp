# -*- coding: utf-8 -*-
"""This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>"""

__author__ = "_GolfLima_"
__copyright__ = "GNU GPL"
__version__ = "0.2.1"

import pygame
import sys
from pygame.locals import *
import yaml
'''Joysticks is create by
Simpson College Computer Science
http://programarcadegames.com/
http://simpson.edu/computer-science/
'''
import joystick

with open(r'parameters.yml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    parameters = yaml.load(file, Loader=yaml.FullLoader)

# Set up pygame
pygame.init()
pygame.mixer.init()
pygame.joystick.init()
joysticks = pygame.joystick.Joystick(parameters['Joystick']['id'])
FPS = parameters['Windows']['fps']
font = pygame.font.Font(pygame.font.get_default_font(), 36)
clock = pygame.time.Clock()

# Set Windows
window_size_x, window_size_y = parameters['Windows']['x'], parameters['Windows']['y']
windowSurface = pygame.display.set_mode((window_size_x, window_size_y), pygame.NOFRAME)
pygame.display.set_caption(parameters['Windows']['title'])

# Sound
sound_alert = pygame.mixer.Sound(parameters['Sound']['file'])


class Axis:
    """
    Axis is object to draw line of a specific axis

    ...

    Attributes
    ----------
    surface :
        pygame surface object to draw line
    color : tuple
        Color of line
    thickness : int
        thickness of line
    start_border: int
        start of line
    array_points : bytearray
        list of points for draw pygame lines
    Methods
    -------
    update(_value):
        draw the line with list of points in array_points
    """

    def __init__(self, _surface, _color, _thickness, _start_border):
        """
        Constructs all the necessary attributes for the Axis object.

        Parameters
        ----------
            _surface :
                pygame surface object to draw line
            _color : tuple
                Color of line
            _thickness : int
                thickness of line
            _start_border: int
                start of line
        """
        self.start_border = _start_border
        self.surface = _surface
        self.color = _color
        self.thickness = _thickness
        self.array_points = [(_start_border, 0), (_start_border, 0)]

    def update(self, _value):
        """
                Draws a line with some points in array_points
                value is new value to add. If a points is out of windows we delete it.

                Parameters
                ----------
                _value : int
                    new value to add in array_points

                Returns
                -------
                None
        """
        global window_size_y
        value = (window_size_y/2) - (_value * (window_size_y/2))  # Pass to -1 - 1 at 0 - 100
        self.array_points.append((self.start_border - 1, value))
        for i, points in enumerate(self.array_points):
            temp = list(self.array_points[i])
            if temp[0] > 0:
                temp[0] -= 1
                self.array_points[i] = tuple(temp)
            else:
                self.array_points.pop(i)  # Delete points out of Windows
        pygame.draw.lines(self.surface, self.color, False, self.array_points, self.thickness)


if __name__ == '__main__':

    axe1 = Axis(windowSurface, tuple(parameters['Color']['AXIS1']), 5, window_size_x)
    axe2 = Axis(windowSurface, tuple(parameters['Color']['AXIS2']), 5, window_size_x)
    value1, value2 = 0, 0
    windows_no_border = True
    error = ""
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == MOUSEBUTTONDOWN:
                pass
            elif event.type == MOUSEMOTION:
                pass
            elif event.type == MOUSEBUTTONUP:
                pass
            elif event.type == KEYDOWN:
                if event.key == pygame.K_KP_ENTER:
                    windows_no_border = False if windows_no_border else True
                if event.key == pygame.K_F1:
                    joystick.information()
            elif event.type == pygame.JOYAXISMOTION:
                try:
                    value1 = joysticks.get_axis(parameters['Joystick']['axis1'])
                    value2 = joysticks.get_axis(parameters['Joystick']['axis2'])
                except pygame.error:
                    error = "Joystick Error ! "
                if parameters['Sound']['axis1'] and 0.05 > value1 > -0.05:
                    sound_alert.set_volume(parameters['Sound']['volume'])
                    sound_alert.play()
                if parameters['Sound']['axis2'] and 0.05 > value2 > -0.05:
                    sound_alert.set_volume(parameters['Sound']['volume'])
                    sound_alert.play()
        # DRAW Windows
        windowSurface = pygame.display.set_mode((window_size_x, window_size_y),
                                                pygame.NOFRAME) if windows_no_border else pygame.display.set_mode(
            (window_size_x, window_size_y))
        windowSurface.fill(tuple(parameters['Color']['BACKGROUND']))
        text_error = font.render(error, True, (255, 0, 0))
        text_error_zone = text_error.get_rect()
        text_error_zone.center = window_size_x / 2, window_size_y / 2
        windowSurface.blit(text_error, text_error_zone)
        pygame.draw.line(windowSurface, (0, 0, 0), (0, 50), (window_size_x, 50), 2)
        pygame.draw.line(windowSurface, (0, 0, 0), (0, 150), (window_size_x, 150), 2)
        axe1.update(value1)
        axe2.update(value2)
        pygame.display.update()
        pygame.display.flip()
        clock.tick(FPS)
