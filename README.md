# Real Time Data Pedals - R.T.D.P

RTDP is a python program with pygame  
https://www.pygame.org  
version : 0.2.1  
See requirements.txt for version of Pygame and other import

![alt text](TelemetryJoystick.png)  

## EN

### Keymap
- F1 : Joystick information for help to find axis.  
- Keypad enter :
    Activate the border of the window, allowing the window to be moved.
    Press it a second time after moving it to return to borderless mode.  
  

### Always on top

For this version we use DeskPins.exe
https://efotinis.neocities.org/deskpins/index.html  

v1.32 exe is include is this source.
Just start DeskPins and pin RTDP windows.

### Make a Windows Executable :

use pyinstaller  https://www.pyinstaller.org/  
>pyinstaller --onefile main.py --windowed -n RTDP

exe version 0.2.1 : https://golflima.s3.fr-par.scw.cloud/RTDP/RTDP.zip  
*No python installation need with this executable file*

###Parameters File 'parameters.yml' : 

>Windows :
  - x: XXX  ``Width of the windows``
  - y: YYY  ``Height of the windows``
  - fps: 40  ``Frame per second for pygame refresh ( 40 works good )``
  - title: My Windows  ``Title of the windows``
>Joystick:  
  - id: 0 ``id for the joystisk to use ``
  - axis1: 1  ``id for axis 1 `` 
  - axis2: 0  ``id for axis 2 ``
>Sound:
  - axis1: 0 ``1- active 0- inactive`` 
  - axis2: 0  ``1- active 0- inactive`` 
  - file : beep.wav  ``path of sound file`` 
  - volume: 0.1 ``Sound volume 0 to 1`` 
>Color:  
  - BACKGROUND: [105, 105, 105]  ``Tuple RGB for Color of Background windows`` 
  - AXIS1: [255, 0, 0] ``Tuple RGB for Color Axis 1`` 
  - AXIS2: [0, 255, 0]  ``Tuple RGB for Color Axis 2`` 

See requirements.txt for version of Pygame and other import

## FR

### Touches
- F1 : Joystick information for help to find axis.
- keypad enter: La touche Enter du pavé numérique est utilisé pour activer la bordure de la fenêtre ce qui permet de déplacer celle-ci.
Appuyez une deuxieme fois après l'avoir déplacée pour repasser en mode sans bordure.

### Fenetre par dessus les autres

Pour cette fonctionnalité par encore implémenté  nous utilisons DeskPins.exe
https://efotinis.neocities.org/deskpins/index.html  

v1.32.exe est inclus dans la source.
Il suffit de la lancer et d épingler la fenêtre  de l application.
Elle restera ainsi au dessus des autres.


### Générer un executable windows :

Utiliser pyinstaller  https://www.pyinstaller.org/  
>pyinstaller --onefile main.py --windowed -n RTDP

###Fichier de configuration  'parameters.yml' : 

>Windows :
  - x: XXX  ``Largeur de la fenêtre``
  - y: YYY  ``Hauteur de la fenêtre``
  - fps: 40  ``Vitesse de rafraichissement de la fenêtre ( 40 est bien)``
  - title: My Windows  ``Titre de la fenêtre``
>Joystick:  
  - id: 0 ``id pour la manette de jeu  ``
  - axis1: 1  ``id pour l axe 1 `` 
  - axis2: 0  ``id pour l' axe 2 ``
>Sound:
  - axis1: 0 ``1- active 0- inactive`` 
  - axis2: 0  ``1- active 0- inactive`` 
  - file : beep.wav  ``Chemin du fichier son utilisé`` 
  - volume: 0.1 ``Volume du son 0 to 1`` 
>Color:  
  - BACKGROUND: [105, 105, 105]  ``Tuple python RVB pour la couleur d'arrière plan`` 
  - AXIS1: [255, 0, 0] ``Tuple python RVB pour la couleur de l'axe 1`` 
  - AXIS2: [0, 255, 0]  ``Tuple python RVB pour la couleur de l'axe 2``